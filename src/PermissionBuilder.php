<?php

declare(strict_types = 1);

namespace Drupal\custom_permissions;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * This class is used to create a list of permissions from config entity.
 *
 * @package Drupal\custom_permissions
 */
class PermissionBuilder implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ContentTranslationPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Create a function to return a list of permissions.
   */
  public function buildPermissions(): array {
    // Create an array to store the permissions.
    $permissions = [];

    // List out all config entities custom_permissions.
    $config_entities = $this->entityTypeManager->getStorage('custom_permissions')->loadMultiple();

    // Loop through all config entities.
    foreach ($config_entities as $entity) {
      // Create a permission for each config entity.
      if ($entity->status()) {
        $permissions[$entity->id()] = [
          'title' => $this->t('Custom Permissions: @entity_label', ['@entity_label' => $entity->label()]),
          'description' => $this->t('@entity_description', ['@entity_description' => $entity->get('description')]),
        ];
      }
    }
    return $permissions;
  }

}
