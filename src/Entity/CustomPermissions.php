<?php declare(strict_types = 1);

namespace Drupal\custom_permissions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\custom_permissions\CustomPermissionsInterface;

/**
 * Defines the custom permissions entity type.
 *
 * @ConfigEntityType(
 *   id = "custom_permissions",
 *   label = @Translation("Custom permissions"),
 *   label_collection = @Translation("Custom permissionss"),
 *   label_singular = @Translation("custom permissions"),
 *   label_plural = @Translation("custom permissionss"),
 *   label_count = @PluralTranslation(
 *     singular = "@count custom permissions",
 *     plural = "@count custom permissionss",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\custom_permissions\CustomPermissionsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\custom_permissions\Form\CustomPermissionsForm",
 *       "edit" = "Drupal\custom_permissions\Form\CustomPermissionsForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "custom_permissions",
 *   admin_permission = "administer custom_permissions",
 *   links = {
 *     "collection" = "/admin/structure/custom-permissions",
 *     "add-form" = "/admin/structure/custom-permissions/add",
 *     "edit-form" = "/admin/structure/custom-permissions/{custom_permissions}",
 *     "delete-form" = "/admin/structure/custom-permissions/{custom_permissions}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 * )
 */
final class CustomPermissions extends ConfigEntityBase implements CustomPermissionsInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $label;

  /**
   * The example description.
   */
  protected string $description;

}
