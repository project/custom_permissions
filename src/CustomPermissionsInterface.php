<?php declare(strict_types = 1);

namespace Drupal\custom_permissions;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a custom permissions entity type.
 */
interface CustomPermissionsInterface extends ConfigEntityInterface {

}
