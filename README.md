# INTRODUCTION

The **Custom Permissions** module provides an interface to define and manage custom permissions that can be used across Drupal. It allows administrators to easily create and manage custom permissions through a dedicated admin page.

The primary use case for this module is:

- Define your own permissions without writing a `permission.yml` file. This is primarily for individuals without expertise in backend development, focusing on front-end work without the need for a custom module. You can add permissions and use them, especially in Views and other modules.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: [Drupal Module Installation Guide](https://www.drupal.org/node/895232) for further information.

## FEATURES

- Define custom permissions.
- Manage custom permissions through an admin interface.
- Integrates seamlessly with Drupal's permission system.
- Designed to be flexible and usable in various parts of Drupal, including Views and other modules.

## USAGE

1. Install and enable the Custom Permissions module.
2. Navigate to the Permissions page (`/admin/people/custom-permissions`).
3. Define and manage your custom permissions.
4. Use the defined custom permissions in various parts of Drupal, such as Views or other modules.
